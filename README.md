## [sheet-scraper.go][repo]

[![pipeline status](https://gitlab.com/aos2/sheet-scraper/badges/master/pipeline.svg)](https://gitlab.com/aos2/sheet-scraper/commits/master)

Golang port of our Python script that facilitates sheet exports by HTTP request. We resort to this as the [Google Drive API][gdrive-api] only allows `text/csv` (and `text/tsv`) export of a [spreadsheet's first sheet](https://developers.google.com/drive/api/v3/manage-downloads#downloading_google_documents) and the separate [Google Sheet API][gsheet-api] doesn't support exports at all. Even if getting `csv`s from arbitrary sheets is not your use case, feel free to study this script as an [extended][gsheet-quickstart] [example][gdrive-quickstart] of authenticating your program against Google and accessing the exposed service.

You can find the [binary executable built from the latest push to master here][release].

## usage

As shipped, the script expects a `credentials.json` for an OAuth client in its directory. This can be generated from the [Google Developer's Console][console] -- use the account that has write access to your desired sheets. The access token will be cached as `token.json` (generate a new one whenever you change scope by e.g., deleting it).

```sh
# These examples use Google's official example spreadsheet.
# https://docs.google.com/spreadsheets/d/1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms/edit#gid=0
./sheet-scraper "1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms" 0 "foo.csv"
./sheet-scraper -d "1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms" "text/csv" "foo.csv"
./sheet-scraper -l="1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms"
# 'Class Data',0
```

### scope

A caveat to our implementation is requiring the read/write scope `https://www.googleapis.com/auth/spreadsheets` for files that are not publically viewable. With read-only access [the request will 401][401]. See more on [Sheets scopes here][gsheet-scope] and [here for Drive scopes][gdrive-scope].

## development

```sh
# You will need to install the following Google libraries.
go get -u golang.org/x/oauth2/google
go get -u google.golang.org/api/drive/v3
go get -u google.golang.org/api/sheets/v4
```

This repo has a pipeline with a linting stage that runs `gofmt`. Consider setting up your editor environment to automatically run `gofmt` on save or use a [pre-commit hook][hook].

[401]: https://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.4.2
[console]: https://console.developers.google.com
[gdrive-api]: https://developers.google.com/drive/api/v3/about-sdk
[gdrive-quickstart]: https://developers.google.com/drive/api/v3/quickstart/go
[gdrive-scope]: https://godoc.org/google.golang.org/api/drive/v3#pkg-constants
[gsheet-api]: https://developers.google.com/sheets/api/
[gsheet-quickstart]: https://developers.google.com/sheets/api/quickstart/go
[gsheet-scope]: https://godoc.org/google.golang.org/api/sheets/v4#pkg-constants
[hook]: https://stackoverflow.com/a/39815997/5255922
[release]: https://gitlab.com/aos2/sheet-scraper/-/jobs/artifacts/master/browse?job=build
[repo]: https://gitlab.com/aos2/sheet-scraper
