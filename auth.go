package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"golang.org/x/net/context"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/drive/v3"
	"google.golang.org/api/sheets/v4"
)

var scopes = []string{drive.DriveReadonlyScope, sheets.SpreadsheetsScope}

func authenticate() (*http.Client, *oauth2.Token) {
	config := readCredentials()
	return getClient(config)
}

func authorize(token *oauth2.Token) func(*http.Request, []*http.Request) error {
	return func(request *http.Request, via []*http.Request) error {
		token.SetAuthHeader(request)
		return nil
	}
}

func getClient(config *oauth2.Config) (*http.Client, *oauth2.Token) {
	tokFile := "token.json"
	tok, err := getTokenFromFile(tokFile)
	if err != nil {
		tok = getTokenFromWeb(config)
		saveToken(tokFile, tok)
	} else {
		tok = refreshToken(config, tok)
	}
	return config.Client(context.Background(), tok), tok
}

func getDriveService(creds *http.Client) *drive.Service {
	driveService, err := drive.New(creds)
	if err != nil {
		log.Fatalf("Unable to retrieve Drive client: %v", err)
	}
	return driveService
}

func getDriveFilesService(service *drive.Service) *drive.FilesService {
	return drive.NewFilesService(service)
}

func getSheetsService(creds *http.Client) *sheets.Service {
	sheetService, err := sheets.New(creds)
	if err != nil {
		log.Fatalf("Unable to retrieve Sheets client: %v", err)
	}
	return sheetService
}

func getTokenFromFile(file string) (*oauth2.Token, error) {
	f, err := os.Open(file)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	tok := &oauth2.Token{}
	err = json.NewDecoder(f).Decode(tok)
	return tok, err
}

func getTokenFromWeb(config *oauth2.Config) *oauth2.Token {
	authURL := config.AuthCodeURL("state-token", oauth2.AccessTypeOffline)
	fmt.Printf("Go to the following link in your browser then type the "+
		"authorization code: \n%v\n", authURL)

	var authCode string
	if _, err := fmt.Scan(&authCode); err != nil {
		log.Fatalf("Unable to read authorization code %v", err)
	}

	tok, err := config.Exchange(oauth2.NoContext, authCode)
	if err != nil {
		log.Fatalf("Unable to retrieve token from web %v", err)
	}
	return tok
}

func readCredentials() *oauth2.Config {
	b, err := ioutil.ReadFile("credentials.json")
	if err != nil {
		log.Fatalf("Unable to read client secret file: %v", err)
	}

	config, err := google.ConfigFromJSON(b, scopes...)
	return config
}

func refreshToken(config *oauth2.Config, oldTok *oauth2.Token) *oauth2.Token {
	tokSource := config.TokenSource(oauth2.NoContext, oldTok)
	newTok, err := tokSource.Token()
	if err != nil {
		log.Fatalf("Unable to refresh token: %v", err)
	}
	if newTok.AccessToken != oldTok.AccessToken {
		fmt.Println("Updating token...")
		saveToken("token.json", newTok)
	}
	return newTok
}

func saveToken(path string, token *oauth2.Token) {
	fmt.Printf("Saving token file to: %s\n", path)
	f, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0600)
	if err != nil {
		log.Fatalf("Unable to cache oauth token: %v", err)
	}
	defer f.Close()
	json.NewEncoder(f).Encode(token)
}
