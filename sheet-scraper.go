package main

import (
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"path/filepath"
	"strings"
	"time"
)

func createRequest(method string, url string, body io.Reader) *http.Request {
	request, err := http.NewRequest(method, url, body)
	if err != nil {
		log.Fatalf("Unable to create request: %v", err)
	}
	return request
}

func download(filePath string, mimeType string, fileId string) {
	creds, _ := authenticate()
	service := getDriveService(creds)
	files := getDriveFilesService(service)
	/* https://godoc.org/google.golang.org/api/drive/v3#FilesService.Export
	   NB: as of writing, exported content is limited to 10MB. */
	request := files.Export(fileId, mimeType)
	response, err := request.Download()
	if err != nil {
		log.Fatalf("Unable to download: %v", err)
	}
	writeFile(filePath, response.Body)
}

func export(filePath string, spreadsheetId string, gid string) {
	creds, token := authenticate()
	service := getSheetsService(creds)
	spreadsheet, err := service.Spreadsheets.Get(spreadsheetId).Do()
	if err != nil {
		log.Fatalf("Unable to retrieve spreadsheet: %v", err)
	}

	fileType := filepath.Ext(filePath)[1:]
	exportUrl := strings.Replace(
		spreadsheet.SpreadsheetUrl, "/edit", "/export", 1)
	exportUrl += "?format=" + fileType + "&gid=" + gid
	request := createRequest(http.MethodGet, exportUrl, nil)
	token.SetAuthHeader(request)
	client := &http.Client{
		CheckRedirect: authorize(token),
		Timeout:       time.Second * 300,
	}
	response, err := client.Do(request)
	if err != nil {
		log.Fatalf("Unable to get spreadsheet: %v", err)
	}
	writeFile(filePath, response.Body)
}

func listSheets(spreadsheetId string) {
	creds, _ := authenticate()
	service := getSheetsService(creds)
	spreadsheet, err := service.Spreadsheets.Get(spreadsheetId).Do()
	if err != nil {
		log.Fatalf("Unable to retrieve spreadsheet: %v", err)
	}
	for _, sheet := range spreadsheet.Sheets {
		props := sheet.Properties
		fmt.Printf("'%s',%v\n", props.Title, props.SheetId)
	}
}

func writeFile(file string, body io.ReadCloser) {
	b, err := ioutil.ReadAll(body)
	if err != nil {
		log.Fatalf("Unable to read request body: %v", err)
	}
	fmt.Printf("Saving file: %s\n", file)
	ioutil.WriteFile(file, b, 0600)
}

func main() {
	downloadFlag := flag.Bool("d", false, `export using Drive API`)
	listFlag := flag.String("l", "", `list all sheets of a spreadsheet`)
	flag.Parse()
	if *downloadFlag {
		fileId, mimeType, filePath := flag.Arg(0), flag.Arg(1), flag.Arg(2)
		download(filePath, mimeType, fileId)
	} else if *listFlag != "" {
		listSheets(*listFlag)
	} else {
		spreadsheetId, fileId, filePath := flag.Arg(0), flag.Arg(1), flag.Arg(2)
		export(filePath, spreadsheetId, fileId)
	}
}
